#include <iostream>

struct A {
	bool f() const; bool g() const;
	virtual std::string bhar()[[expects::f() && g()]];
	virtual int hash()[[ensures::g()]];
	virtual void gash()[[expects::g()]];
	virtual double fash(int i) const[[expects:i > 0]];
};

struct B : A {
	std::string bhar() override[[expects:f()]]
};


//Visual studio 에서 지원을 못해주는건지 않됨;;;