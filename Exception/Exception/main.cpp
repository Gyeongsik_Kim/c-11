#include <iostream>

int main(void) {
	int a = 123;
	try {
		if (a > 0)
			throw "test";
	} catch (char* e) {
		std::cout << e << std::endl;
	}
	system("pause");
	quick_exit(EXIT_SUCCESS);
}
