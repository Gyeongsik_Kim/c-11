#include <iostream>
#include <vector>

using namespace std;

int main(void) {
	vector<int> v1;
	vector<int> v2;

	v1.push_back(12);
	v1.push_back(450);

	//v1.swap(v2);
	swap(v1, v2);

	for each(auto i in v2) {
		cout << i << endl;
	}
	return 0;
}