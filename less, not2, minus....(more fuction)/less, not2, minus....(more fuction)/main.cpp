#include <iostream>
#include <functional>

using namespace std;

int main (void) {
	cout << less<int> ()(10, 40) << endl;
	cout << less<int> ()(40, 40) << endl;
	cout << less<int> ()(40, 10) << endl;
	cout << endl;

	cout << greater<int> ()(10, 40) << endl;
	cout << greater<int> ()(40, 40) << endl;
	cout << greater<int> ()(40, 10) << endl;
	cout << endl;
	
	cout << plus<double> ()(10.56, 40) << endl;

	cout << plus<int> ()(40, 40) << endl;
	cout << plus<int> ()(40, 10) << endl;
	cout << endl;
	
	cout << minus<int> ()(10, 40) << endl;
	cout << minus<int> ()(40, 40) << endl;
	cout << minus<int> ()(40, 10) << endl;
	cout << endl;


	cout << not2(less<int> ())(10, 40) << endl;
	cout << not2(less<int> ())(40, 40) << endl;
	cout << not2(less<int> ())(40, 10) << endl;
	cout << endl;

	cout << not2(greater<int> ())(10, 40) << endl;
	cout << not2(greater<int> ())(40, 40) << endl;
	cout << not2(greater<int> ())(40, 10) << endl;
	cout << endl;
	
}