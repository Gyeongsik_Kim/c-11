#include <thread>
#include <string>
#include <iostream>

class TEST{
	public:
		TEST(const std::string& text){
			temp = text;
		}
		void operator()(const std::string& arg) const{
			std::cout << temp << std::endl;
			std::cout << arg << std::endl;
		}
		void Print() const{
			std::cout << temp << std::endl;
		}
	private:
		std::string temp;
};

void main(){
	
	
	auto th1 = std::thread(TEST("TEST!"), "GD"); //인자 두개일 경우에는 operator 도 실행됨
	th1.join();

	TEST c("C++11 thread"); //TEST 생성자 실행
	auto th2 = std::thread(&TEST::Print, c); //c는 식별자라고 함
	th2.join();
}