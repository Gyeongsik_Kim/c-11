#include <functional> // std::function
#include <iostream> //cout

std::function< int(int, float, double) > func = [](int a, float b, double c) -> int {
	printf("a = %d, b = %f, c = %f\n", a, b, c);
	return a;
};

void main(void) {
	int a = func(123, 123.321, 412.31241);
	std::cout << "Return : " << a << std::endl;
	return;
	
	
}	