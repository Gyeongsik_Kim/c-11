#include <chrono>
#include <iostream>

using namespace std;
using namespace chrono;

void BigLoop(){
	for (uint32_t i = 1; i < 100; i++){
	}
}

int main() {
	// now() 함수를 통해 현재 시간값을 구한다.
	system_clock::time_point start = system_clock::now();
	BigLoop();

	system_clock::time_point end = system_clock::now();

	// 초 단위 (소수점으로 표현)
	duration<double> sec = end - start;

	//다른 형태로 변환을 위해서는 duration_cast를 사용해야됨
	nanoseconds nano = duration_cast<nanoseconds>(sec); 

	cout << "초 단위 : " << sec.count() << endl; //count() 함수는 숫자로 변환해줌
	cout << "나노 초 단위 : " << nano.count() << endl;
	system("pause");

}