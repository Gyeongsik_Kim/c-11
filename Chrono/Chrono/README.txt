이전의 타이머
#include <Windows.h>
#pragma comment(lib, "winmm.lib")


timeBeginPeriod(1);

// ...timeGetTime 사용

// timeEndPeriod의 인자값은 timeBeginPeriod에서 설정한 인자값과 동일해야 한다
timeEndPeriod(1);
약 5ms 정도 차이남

C++11에 와서는 chrono 가 생겨남


chrono 는 다음과 같은 구조로 이루어져 있기 때문에 
/-
namespace std
{
    namespace chrono
    {
    }
}
*-

using namespace std;
using namespace chrono;

와 같이 사용하면 쉽게 쓸수 있다.