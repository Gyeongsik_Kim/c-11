#include <iostream>
#include <stdlib.h>
#include <cassert>
	
void test(int *p) {
	assert(p != nullptr);	
	std::cout << *p << std::endl;
}

int main(void) {

	int a = 10;
	int * b = nullptr;
	int * c = nullptr;

	b = &a;

	test(b);
	test(c);


	std::quick_exit(EXIT_SUCCESS);
}