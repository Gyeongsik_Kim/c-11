#include <iostream>
#include <vector>

struct simple {
	int a;
	int b;
	operator double() {
		return (double)a / b;
	}
};

int main(void) {
	simple s{ 1 ,2 };
	std::cout << s << std::endl;
	quick_exit(EXIT_SUCCESS);
}