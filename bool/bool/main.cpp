#include <iostream>

int main (void) {
	bool test = true;
	std::cout.setf (std::ios::boolalpha);
	std::cout << test << std::endl;
	std::cout << !test << std::endl;

	std::cout.unsetf (std::ios::boolalpha);
	std::cout << test << std::endl;
	std::cout << !test << std::endl;

	return EXIT_SUCCESS;
}