#include <random>
#include <chrono>
#include <set>
#include <iostream>
/* 
C++11 랜덤 함수
Generators 균일하게 분포된 숫자를 생성
Distributions 특정 분포에 맞춰서 생성된 숫자를 변환
*/
int main()
{
	auto current = std::chrono::system_clock::now();
	auto duration = current.time_since_epoch();
	auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

	std::mt19937_64 genMT(millis);
	std::uniform_int_distribution<__int64> uniformDist(1, 45);

	std::cout << "Min Value : " << uniformDist.min() << std::endl;
	std::cout << "Max Value : " << uniformDist.max() << std::endl;

	// Make 6 random numbers.
	std::set<__int64> stLotto;
	while (6 > stLotto.size())
	{
		stLotto.insert(uniformDist(genMT));
	}

	// print 6 Numbers.
	for (auto nVal : stLotto)
	{
		std::cout << nVal << std::endl;
	}

	return 0;
}