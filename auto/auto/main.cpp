#include <iostream>
#include <list>
#include <string>

char* randomString (const int);

int main (void) {
	std::list<std::string> t;
	for (auto i = 0; i < 20; i++) 
		t.push_back (randomString (10));
	//무작의 데이터 설정

	for (auto test : t)
		std::cout << test <<  std::endl;
}

char* randomString (const int len) {
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
	char* result = (char*)malloc (len);
	for (int i = 0; i < len; ++i) {
		result[i] = alphanum[rand () % (sizeof (alphanum) - 1)];
	}

	result[len] = 0;

	return result;
}