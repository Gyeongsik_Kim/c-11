HASH MAP과 MAP 차이점

HASH MAP 은 빠른 탐색 시간을 보여주지만, 정렬이 되지 않아서 탐색 성능이 일정하지 않음
(그래서 C++ 정식에 포함 않됨)


Hash Map
	자료 탐색에 Hashing 사용
	탐색 속도 O(1) 이상 : Key값 분포에 따라 다름
	저장된 내부 자료 정렬하지 않음
	

Map 
	자료 탐색에 이진 탐색 트리 사용 (최근엔 Red-Black Tree)
	탐색 속도 O(log n) 보장 
	저장된 내부 자료 정렬 