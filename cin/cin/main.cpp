#include <iostream>

using namespace std;
/*
	cin 입력 오류 해결 과정
	1. fail을 통해서 오류가 났는지 검사
	2. clear로 에러 상태 취소 
	3. ignore을 통해서 버퍼 정리
*/
int main(void) {
	int test;
	cin >> test;
	/* 두개 이상 입력시
		int test, test2;
		cin >> test >> test2;
	*/
	if (cin.fail()) {
		cout << "잘못된 입력입니다" << endl;
		cin.clear();
		cin.ignore(UINT64_MAX, '\n');
	}

	system("pause");
	
}