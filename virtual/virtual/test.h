#pragma once
#include <iostream>

class t {
	public :
		virtual int test1() = 0;
		virtual int test2() = 0;
		virtual int test3() = 0;
		virtual int test4() = 0;
		virtual int test5() = 0;
};
//Ctrl + .
//Shift + Alt
class t2 : public t {
	public : 
		virtual int test1();
		virtual int test2();
		virtual int test3();
		virtual int test4();
		virtual int test5();
};

int main(void);
	