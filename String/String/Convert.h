#pragma once
#include <string>
#include <codecvt>
#include <locale>
#include <iostream>

/* 
Not work
std::u32string toUTF32(std::string str) {
	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> cvt;
	return cvt.from_bytes(str);
}
*/

std::u32string toUTF32(std::string str) {
	std::u32string s32;
	s32.resize(str.length());
	std::copy(str.begin(), str.end(), s32.begin());
	return s32;
}
