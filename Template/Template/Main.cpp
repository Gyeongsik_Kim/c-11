#include <iostream>


template <typename test>
class Data {
private :
	test data;
public:
	Data(test data) : data(data){}
	void Showinfo() {
		std::cout << data << std::endl;
	}
};

int main(void){
	Data<int> data1(50);
	data1.Showinfo();

	Data<double> data2(52.31);
	data2.Showinfo();
}