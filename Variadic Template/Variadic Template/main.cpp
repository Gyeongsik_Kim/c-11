#include <iostream>
void print(const char *s){
	while (*s) {
		if (*s == '%') {
			if (*(s + 1) == '%') {
				++s;
			} else {
				throw std::runtime_error("invalid format string: missing arguments");
			}
		}
		std::cout << *s++;
	}
}

template<typename T, typename... Args>
void print(const char *s, T value, Args... args){
	while (*s) {
		if (*s == '%') {
			if (*(s + 1) == '%') {
				++s;
			} else {
				std::cout << value;
				s += 2; 
				print(s, args...); 
				return;
			}
		}
		std::cout << *s++;
	}
}

int main(void) {
	print("test");
	quick_exit(EXIT_SUCCESS);
}