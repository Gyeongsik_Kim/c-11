#include <random>
#include <iostream>
#include <conio.h>
#include <algorithm>
#include <string>
#include <assert.h>
#include <crtdbg.h>

#define MAX 5
int main(void) {
	//메모리 누수 방지
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	//랜덤 준비
	std::random_device rd;
	std::mt19937_64 fd(rd());

	//벡터 선언 및 초기화
	std::vector<int> a;
	for (int i = 0; i < MAX; i++)
		a.push_back(i);

	//Random swap and show data
	while (1) {
		std::swap(a[fd() % 5], a[fd() % 5]);
		if (_kbhit()) {
			if (_getch() == 13) {
				system("cls");
				uint8_t n = 0;
				for each(auto i in a)
					std::cout << std::to_string(++n) << "번팀 -> " << ++i << "로" << std::endl;
			}
		}
	}

	quick_exit(EXIT_SUCCESS);
}